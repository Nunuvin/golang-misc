/*
    Lesser LS in go
*/
package main
import(
    "fmt"
    "os"
)

func main(){
    s, _ := os.Getwd()
    fmt.Println(s)
    f, _ := os.Open(s)
    files, _ := f.Readdirnames(0)
    for _, f := range files{
//        fmt.Println(f)
        st, _ := os.Stat(f)
        fmt.Println(st.Mode(), st.ModTime(), st.Size(), st.Name())
    }

}
